"""Configure a Ganga job for creating ntuples from Turbo data."""
from __future__ import division, print_function

import argparse
import getpass
import os
import sys
sys.path.insert(0, '.')

from streams import turbo_streams

########################################
# User configuration begins
########################################

hlt2_line_name = 'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0DD_LTUNBTurbo'

########################################
# User configuration ends
########################################

# Figure out what stream the line is in
stream = None
for s, lines in turbo_streams.items():
    if hlt2_line_name in lines:
        stream = s
if stream is None:
    print('Could not find line {0} in any stream, aborting'.format(
        hlt2_line_name
    ))
    sys.exit(1)


parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('year', type=int, choices=[2016],
                    help='Year of data-taking to run over')
parser.add_argument('polarity', choices=['MagDown', 'MagUp'],
                    help='Polarity of data-taking to run over')
parser.add_argument('--test', action='store_true',
                    help='Run over one file locally')
args = parser.parse_args()

year = args.year
polarity = args.polarity
test = args.test

print('Found line {0!r} in Turbo stream {1!r}'.format(
    hlt2_line_name, stream
))

# The stream definitions are in:
# DBASE/TurboStreamProd/python/TurboStreamProd/streams.py
bkpath = '/LHCb/Collision16/Beam6500GeV-VeloClosed-{0}/Real Data/Turbo03/94000000/{1}.MDST'
bkpath = bkpath.format(polarity, stream)

bkq = BKQuery(bkpath)
ds = bkq.getDataset()
if ds.isEmpty():
    print('No files found at bookkeeping path {0}'.format(bkpath))
    sys.exit(1)

j = Job()
j.name = 'CharmTurboNtuples_DV_{0}_{1}_{2}'.format(year, polarity, stream)
dv_version = 'v41r2'
# Create a CMake-style application if one doesn't already exist
if os.path.exists('./DaVinciDev_{0}'.format(dv_version)):
    j.application = GaudiExec(directory='./DaVinciDev_{0}'.format(dv_version))
else:
    j.application = prepareGaudiExec('DaVinci', dv_version, myPath='.')
j.application.options = [
    'decaytreetuples.py'
]
if test:
    # Run over a single file locally
    j.backend = Local()
    j.inputdata = ds[:1]
else:
    # Run over 1000 files on the grid
    j.backend = Dirac()
    j.parallel_submit = True
    j.do_auto_resubmit = True
    j.inputdata = ds[:1000]
    # 5 files already takes some time
    j.splitter = SplitByFiles(filesPerJob=5)
    # Send status emails to the user submitting the jobs
    uname = getpass.getuser()
    j.postprocessors = [Notifier(address='{0}@cern.ch'.format(uname))]
    j.merger = RootMerger(files=['DVntuple.root', 'DVhistogram.root'],
                          args='-ff')

j.outputfiles = [LocalFile('*.root')]
