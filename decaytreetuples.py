"""Configure DaVinci for creating ntuples from Turbo data."""
from Configurables import DaVinci, DecayTreeTuple
from DecayTreeTuple import Configuration
from PhysConf.Filters import LoKi_Filters

########################################
# User configuration begins
########################################

# The full name of the HLT2 line
hlt2_line_name = 'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0DD_LTUNBTurbo'

# The decay descriptor, including string template markers to indicate which
# particles to save information about, and what names the brances should be
# called
decay_descriptor_template = '${Dst}[D*(2010)+ -> ${D0}(D0 -> ${D0_KS}KS0 ${D0_pim}pi- ${D0_pip}pi+) ${Dst_pi}pi+]CC'

########################################
# User configuration ends
########################################

# Method added to DecayTreeTuple by Olli Lupton, available from DaVinci v42r0
# See merge request Analysis!47
def setDescriptorTemplate(dtt, template):
    from string import Template

    dd = Template(template)
    particles = [
        y[1] if len(y[1]) else y[2]
        for y in dd.pattern.findall(dd.template)
        if len(y[1]) or len(y[2])
    ]
    mapping = { p : '^' for p in particles }
    mapping[particles[0]] = ''
    dtt.Decay = dd.substitute(mapping)
    branches = { }
    for p in particles:
        mapping = { q : '^' if p == q else '' for q in particles }
        branches[p] = dd.substitute(mapping)
    return dtt.addBranches(branches)

# DecayTreeTuple input location template
turbo_input = '/Event/Turbo/{0}/Particles'

# Give the DecayTreeTuple a meaningful name, using the HLT2 line name
name = hlt2_line_name.replace('CharmHad', '').replace('Turbo', '')
dtt = DecayTreeTuple('{0}_Tuple'.format(name))
dtt.WriteP2PVRelations = False
dtt.InputPrimaryVertices = '/Event/Turbo/Primary'
dtt.Inputs = [turbo_input.format(hlt2_line_name)]
setDescriptorTemplate(dtt, decay_descriptor_template)

# Tuple tools that we don't need to configure and that apply to all
# branches
dtt.ToolList = [
    'TupleToolEventInfo',
    'TupleToolGeometry',
    'TupleToolKinematic',
    'TupleToolPid',
    'TupleToolPrimaries',
    'TupleToolTrackInfo'
]

# Reconstruction information
recostats = dtt.addTupleTool('TupleToolRecoStats')
recostats.Verbose = True

# Add TISTOS information for lines commonly used for charm
tistos = dtt.addTupleTool('TupleToolTISTOS')
tistos.TriggerList = [
    'L0HadronDecision',
    'Hlt1TrackMVADecision',
    'Hlt1TwoTrackMVADecision'
]
# HLT2 selection reports aren't present in 2016 data
tistos.FillHlt2 = False
tistos.Verbose = True

# If we have a D* to D0 pi decay, add the delta mass variable computed with and
# without DecayTreeFitter
if 'D*(2010)' in decay_descriptor_template:
    from string import Template

    # We want to attach the DTF tool only to the head of the decay, so use so
    # template magic to get the first template parameter, assuming the first
    # template parameter is attached to the decay head
    t = Template(decay_descriptor_template)
    head = filter(None, t.pattern.findall(t.template)[0])[0]

    dmtt = getattr(dtt, head).addTupleTool('LoKi::Hybrid::TupleTool/DeltaMass')
    dm_functor = "M - CHILD(M, '[D*(2010)+ -> ^D0 pi+]CC')"
    dmtt.Variables = {
        'delta_M': dm_functor,
        'DTFNoConst_delta_M': 'DTF_FUN({0}, False)'.format(dm_functor),
        'DTFPVConst_delta_M': 'DTF_FUN({0}, True)'.format(dm_functor),
        'DTFPVD0Const_delta_M': "DTF_FUN({0}, True, 'D0')".format(dm_functor)
    }

ntuples = [dtt]

# Speed up processing by filtering out events with no positive signal decisions
trigger_filter = LoKi_Filters(
    HLT2_Code="HLT_PASS_RE('.*{0}.*')".format(hlt2_line_name)
)

dv = DaVinci()
dv.EventPreFilters = trigger_filter.filters('TriggerFilters')
dv.UserAlgorithms = ntuples
dv.TupleFile = 'DVntuple.root'
dv.PrintFreq = 1000
dv.DataType = '2016'
dv.Lumi = True
