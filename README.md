# Options files for Turbo ntuples [![build status](https://gitlab.cern.ch/apearce/charm-turbo-options/badges/master/build.svg)](https://gitlab.cern.ch/apearce/charm-turbo-options/commits/master)

Make ntuples from Turbo data, help reduce the HLT2 bandwidth 💥

We have a problem in the Charm working group: we do so much fantastic physics, 
that we haven't had time to look at most of the Run 2 data yet! So it turns out 
that a lot of our HLT2 lines accept a large amount background, and suffer from 
a low purity. Mass plots of the output of nearly all charm HLT2 lines in 2016 
data are [available to see online][turbo-output].

You can help to reduce the background rate by looking at the mass plots, seeing 
what lines are dominated by background, and optimising a selection using 
ntuples that _this_ repository will help you create.

Usage
-----

There is a single DaVinci options file, `decaytreetuples.py`, and a script to 
set up jobs in Ganga for submission locally and to the Grid, `ganga_job.py`.
You need to edit each of these with the HLT2 line names you'd like to study, 
and should be working on a machine with access to the standard LHCb software, 
such as lxplus. This section will walk you through getting up and running.

To begin, clone this repository and enter the directory that's created.

```shell
$ git clone https://:@gitlab.cern.ch:8443/apearce/charm-turbo-options.git
$ cd charm-turbo-options
```

### DaVinci configuration

Change the default line name and decay descriptor in `decaytreetuples.py` to 
something relevant to what you want to study. The list of charm HLT2 lines is 
listed on [the Turbo output plots page][turbo-output]. The lines you need to 
edit are demarcated with unreasonably large comment headers.

```python
########################################
# User configuration begins
########################################

# The full name of the HLT2 line
hlt2_line_name = 'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0DD_LTUNBTurbo'

# The decay descriptor, including string template markers to indicate which
# particles to save information about, and what names the brances should be
# called
decay_descriptor_template = '${Dst}[D*(2010)+ -> ${D0}(D0 -> ${D0_KS}KS0 ${D0_pim}pi- ${D0_pip}pi+) ${Dst_pi}pi+]CC'

########################################
# User configuration ends
########################################
```

The `decay_descriptor` uses a magic syntax that takes care of setting the both 
the `Decay` and `Branches` properties of the `DecayTreeTuple` that will be 
created.

The special-looking `${name}` syntax means "mark this particle to be saved, and 
prefix the associated branches in the ntuple with `name`". As an example, take 
this decay descriptor template:

```python
decay_descriptor_template = '${D0}[D0 -> ${D0_KS}KS0 ${D0_pim}pi- ${D0_pip}pi+]CC'
```

If you were to get rid of the special `${…}` parts, it would look like your 
standard decay descriptor:

```
[D0 -> KS0 pi- pi+]CC'
```

The addition of the `${…}` parts means you'll end up with the branches for the 
D0 beginning with `D0_`, for the KS0 beginning with `D0_KS_`, and so on. Of 
course, the names you put should be unique within the decay.

Once you've changed the `hlt2_line_name` and `decay_descriptor_template` 
variables, the DaVinci configuration is done.

### Ganga configuration

Because different HLT2 lines go to different streams (like in the Stripping), 
the Ganga configuration in `ganga_job.py` needs to know the HLT2 line name so 
that it run over the right bookkeeping location.

Again, the code you need to edit is highlighted with comment blocks.

```python
########################################
# User configuration begins
########################################

hlt2_line_name = 'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0DD_LTUNBTurbo'

########################################
# User configuration ends
########################################
```

Use the same value as in `decaytreetuples.py`.

### Job submission

Once both files have been edited, test them by creating a job that will run 
locally:

```shel
$ lb-run Ganga/v603r1 ganga -i ganga_job.py 2016 MagUp --test
```

As you might guess, `2016` and `MagUp` define the dataset to run over. The 
`--test` flag configures Ganga to run over only a single file from the Turbo 
stream corresponding to your HLT2 line, and to run DaVinci on the machine you 
run Ganga on. Without the `--test` flag, Ganga will prepare the job ready for 
submission to the Grid, running over the first 1000 files retrieved from the 
bookkeeping. If you're working with a low-rate line, you may wish to run over 
more files.

Once Ganga has started, you can inspect the job, available as the variable `j`, 
and if everything looks OK you can submit it:

```
In [1]: j
...

In [2]: j.submit()
```

Once the job has completed, the output file, called `DVntuple.root`, will be 
visible when you `j.peek()`.

Check the logfiles to make sure everything looks reasonable. If the ntuple is 
empty, check you spelt the HLT2 line name correctly. If in doubt, copy and 
paste the name from the [output plots page][turbo-output], remembers to prepend 
`Hlt2CharmHad` and append `Turbo`.

## Asking questions and reporting problems

If you have any questions, or want to report any problems, please [open an 
issue][issues] on this repository.

## Lines currently under study

The lines listed below are already under investigation. (The `Hlt2CharmHad` 
prefix and `Turbo` suffix have been omitted for clarity.) The investigators are 
listed alongside the lines; please contact them if you have specific concerns.

* `Dstp2D0Pip_D02KmKmKpPip`, `Dstp2D0Pip_D02KmKpKpPim`, 
  `Dstp2D0Pip_D02KmKpPimPip`, `Dstp2D0Pip_D02KmPimPipPip`, 
  `Dstp2D0Pip_D02KpPimPimPip`, `Dstp2D0Pip_D02PimPimPipPip` (Maurizio 
  Martinelli)
* `Dp2KS0KmKpPip_KS0DD`, `Dp2KS0KmKpPip_KS0LL`, `Dp2KS0KmPipPip_KS0DD`, 
  `Dp2KS0KmPipPip_KS0LL`, `Dp2KS0KpKpPim_KS0DD`, `Dp2KS0KpKpPim_KS0LL`, 
  `Dp2KS0KpPimPip_KS0DD`, `Dp2KS0KpPimPip_KS0LL`, and their `Dsp` counterparts 
  (Maurizio Martinelli)
* `Dp2KS0KS0Kp`, `Dp2KS0KS0Pip`, `Dsp2KS0KS0Kp`, `Dsp2KS0KS0Pip`, and their 
  `Dsp` counterparts (Maurizio Martinelli)
* `Dstp2D0Pip_D02KS0KS0_KS0DD`, `Dstp2D0Pip_D02KS0KS0_KS0LL`, 
  `Dstp2D0Pip_D02KS0KS0_KS0LL_KS0DD` (Simone Stracka)

## High priority lines

Coming soon! ⌛️

[turbo-output]: https://apearce.web.cern.ch/apearce/charm-hlt2-histograms/
[issues]: https://gitlab.cern.ch/apearce/charm-turbo-options/issues
